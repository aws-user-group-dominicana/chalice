# chalice

## helloworld tutorial

### Requirements

Install awd cli 2 from https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html

### Install chalice

To install Chalice, we’ll first create and activate a virtual environment in python3.7:
```
$ python3 --version
Python 3.7.3
$ python3 -m venv venv37
$ . venv37/bin/activate
```

Next we’ll install Chalice using pip:

```
$ python3 -m pip install chalice
```

You can verify you have chalice installed by running:

```
$ chalice --help
Usage: chalice [OPTIONS] COMMAND [ARGS]...
```


### Credentials

Before you can deploy an application, be sure you have credentials configured. If you have previously configured your machine to run boto3 (the AWS SDK for Python) or the AWS CLI then you can skip this section.

If this is your first time configuring credentials for AWS you can follow these steps to quickly get started:

```
$ mkdir ~/.aws
$ cat >> ~/.aws/config
[default]
aws_access_key_id=YOUR_ACCESS_KEY_HERE
aws_secret_access_key=YOUR_SECRET_ACCESS_KEY
region=YOUR_REGION (such as us-west-2, us-west-1, etc)
```

### Creating Your Project

The next thing we’ll do is use the chalice command to create a new project:

```
chalice new-project helloworld
```

This will create a helloworld directory. Cd into this directory. You’ll see several files have been created for you:

```
$ cd helloworld
$ ls -la
drwxr-xr-x   .chalice
-rw-r--r--   app.py
-rw-r--r--   requirements.txt
```

Let’s take a look at the app.py file:

```python
from chalice import Chalice

app = Chalice(app_name='helloworld')


@app.route('/')
def index():
    return {'hello': 'world'}
```

The new-project command created a sample app that defines a single view, /, that when called will return the JSON body {"hello": "world"}.

### Deploying

```
$ chalice deploy
Creating deployment package.
Creating IAM role: helloworld-dev
Creating lambda function: helloworld-dev
Creating Rest API
Resources deployed:
  - Lambda ARN: arn:aws:lambda:us-west-2:12345:function:helloworld-dev
  - Rest API URL: https://abcd.execute-api.us-west-2.amazonaws.com/api/
```

You now have an API up and running using API Gateway and Lambda:

```
$ curl https://qxea58oupc.execute-api.us-west-2.amazonaws.com/api/
{"hello": "world"}
```

Try making a change to the returned dictionary from the index() function. You can then redeploy your changes by running chalice deploy.

### Cleaning Up

```
$ chalice delete
Deleting Rest API: abcd4kwyl4
Deleting function aws:arn:lambda:region:123456789:helloworld-dev
Deleting IAM Role helloworld-dev
```

# More tutorial

https://aws.github.io/chalice/tutorials/index.html




