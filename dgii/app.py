from chalice import Chalice
from stdnum.do import rnc

app = Chalice(app_name='dgii')
app.debug = True

@app.route('/')
def index():
    return ({"Hello": 'rnc'})

@app.route('/rnc/{rnc_number}', methods=['GET'], cors=True)
def rnc(rnc_number):
    return rnc.check_dgii(rnc_number)